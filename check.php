<?php
include 'config/payload_.php';
$mes = array(
    "01" => "ENERO","02" => "FEBRERO",
    "03" => "MARZO","04" => "ABRIL",
    "05" => "MAYO","06" => "JUNIO",
    "07" => "JULIO","08" => "AGOSTO",
    "09" => "SETIEMBRE","10" => "OCTUBRE",
    "11" => "NOVIEMBRE","12" => "DICIEMBRE"
);
// $mes = array(
//     "1" => "ENERO","2" => "FEBRERO",
//     "3" => "MARZO","4" => "ABRIL",
//     "5" => "MAYO","6" => "JUNIO",
//     "7" => "JULIO","8" => "AGOSTO",
//     "9" => "SETIEMBRE","1" => "OCTUBRE",
//     "11" => "NOVIEMBRE","12" => "DICIEMBRE"
// );
	$con = new payload_();
	// $producto     =  $_POST["dato"];
	// echo json_encode($_POST['rdTipoBusqueda']);

	if($_POST['rdTipoBusqueda']=='inscripcion')
	{
		$data = $con->showInscripcion($_POST['dato']);
		// echo json_encode($data[1])=='"pagado"';
		// echo ("<br>");
		// echo json_encode($data[0]['FacTotal']);
	}
	else
	{
		$data = $con->showMedidor($_POST['dato']);
		// echo 'es medidor';
	}
	// $list=$con->traer();
	// $data = $con->show($_POST['dato']);
	$ban = count($data);
	$registros = json_encode($data);

	if(count($data)!=0)
	{
		$onlyDate = explode('/',explode('-',str_replace(' ', '-', trim($data[0]['FacEmiFec'])))[0]);
		$periodo = date(trim($data[0]['FacEmiFec']));//aka esta la fecha real solo pruebas
		$periodo = $mes[$onlyDate[1]].' '.$onlyDate[2];
		// echo $onlyDate[0].'/'.$mes[$onlyDate[1]-1].'/'.$onlyDate[2];
		$fechaVence = $onlyDate[0].' / '.$mes[$onlyDate[1]].' / '.$onlyDate[2];
	}

	// echo $ban;
	// echo json_encode($registros);
	// echo $reg->inscripcion;

	$dato = $_POST['dato'];
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
     <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" href="plugins/fontawesome/all/all.min.css"/>
    <link rel="stylesheet" href="asset/main.css">
    <title>EMUSAP S.A</title>
    <link rel="stylesheet" href="assets/css/main.css">
</head>
<body style="background: url('img/bgg.jpg') repeat scroll 50% 0;">
	<?php
	include 'plantilla/header.php';
	?>
	<div class="overlayPagina" style="display: none;">
	    <div class="loadingio-spinner-spin-i3d1hxbhik m-auto">
	        <div class="ldio-onxyanc9oyh">
	            <div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div>
	        </div>
	    </div>
	</div>
  	<div class="container-fluid" style="height: 100vh;">
  		<div class="row justify-content-center" style="height: 100vh;">
  			<div class="col-md-8">
  				<div class="row my-3">
  					<div class="col-4  text-start">
						<span class="badge bg-secondary customer-timeline text-white">1</span>
						<span class="fw-bold">Consulta</span>
					</div>
					<div class="col-4  text-center">
						<span class="badge bg-primary customer-timeline text-white">2</span>
						<span class="fw-bold">Verifica</span>
					</div>
					<div class="col-4  text-end">
						<span class="badge bg-secondary customer-timeline text-white">3</span>
						<span class="fw-bold">Confirma y Paga</span>
					</div>
	  			</div>
	  			<div class="card shadow-lg">
					<div class="card-header">
						<div class="card-title m-0">
							<h3 class="m-0"><i class="fa fa-list"></i> Resultado</h3>
						</div>
					</div>
					<!-- <h1><?php echo count($data); ?></h1>
					<h1><?php echo $ban; ?></h1> -->
		  			<div class="card-body">
		  				<div class="alert alert-warning" style="display: <?php echo $ban==0?'block':'none'; ?>">
		  					<p class="m-0 text-center">No se encontro pagos pendientes con el numero <b><?php echo $dato; ?></b>.</p>
		  				</div>
		  				<div class="row justify-content-center" style="display: <?php echo $ban==0?'none':'block'; ?>">
		  					<?php 
			  				if (isset($data[1])) 
			  				{
			                    if (json_encode($data[1])=='"pagado"') 
			                    {
		                    ?>
		                    <div class="alert alert-success">
			  					<p class="m-0 text-center">Recibo <b>pagado</b>.</p>
			  				</div>
		                    <?php 
		                    	}
		                    }
		                    ?>
		                    
	                    	<div class="alert alert-secondary">
	                    		<div class="row">
	                    			<div class="col-md-6">
	                    				<p class="m-0"><b>Cliente:</b> <span class="name"></span></p>
				  						<p class="m-0"><b>Direccion:</b> <span class="address"></span></p>
				  						<p class="m-0"><b>Medidor:</b> <span class="measurer"></span></p>
				  						<p class="m-0" style="display: none;"><b>t:</b> <span class="test"></span></p>
		                    		</div>
		                    		<div class="col-md-6">
	                    				<p class="m-0"><b>Deuda pendiente:</b> <span class="d1 badge bg-light text-dark"></span></p>
				  						<p class="m-0"><b>Cuotas Fraccionadas:</b> <span class="d2 badge bg-light text-dark"></span></p>
				  						<p class="m-0"><b>Deuda del Mes:</b> <span class="d3 badge bg-light text-dark"></span></p>
		                    		</div>
	                    		</div>	
		  						<form id="form" action="summary.php" method="post" class="m-0">
		  							<input type="hidden" id="medidor" name="medidor" class="medidor">
		  							<input type="hidden" id="amount" name="amount" class="amount">
		  							<input type="hidden" id="id" name="id" class="id">
		  							<!-- <input type="hidden" id="inscripcion" name="inscripcion" class="inscripcion"> -->
		  						</form>
		  					</div>
		  					
			  				<!-- <div class="col-md-7">
			  					<h1><?php echo $dato; ?></h1>
			  				</div> -->
			  				<div class="container-fluid table-responsive p-0">
			  					<table class="table table-bordered table">
			  						<thead>
			  							<tr class="text-center">
			  								<th>Ver</th>
			  								<th>Recibo</th>
			  								<th>Periodo</th>
			  								<th>Vence</th>
			  								<th>Estado</th>
			  								<!-- <th>csa</th>
			  								<th>Deuda anterior</th>
			  								<th>Deuda del mes</th> -->
			  								<th>Total</th>
			  							</tr>
			  						</thead>
			  						<tbody>
			  							<tr class="text-center" style="background: rgb(127 213 153 / 57%);">
			  								<td><button class="btn btn-sm btn-info py-0 text-dark fw-bold">Ver Recibo</button></td>
			  								<td><span class="numRecibo fw-bold"></span></td>
			  								<td><span class="periodo"></span></td>
			  								<td><span class="fechaVence"></span></td>
			  								<td class="estadoRecibo"></td>
			  								<!-- <td>vsd</td>
			  								<td>csa</td>
			  								<td>csa</td> -->
			  								<td class="fw-bold">S/ <span class="total"></span></td>
			  							</tr>
			  						</tbody>
			  					</table>
			  				</div>
		  				</div>

		  			</div>
		  			<div class="card-footer">
		  				<div class="row">
		  					<div class="col-6 text-start">
		  						<a href="index.php" class="btn btn-secondary"><i class="fa fa-arrow-left"></i> Volver</a>
		  					</div>
		  					<?php 
		  					if (isset($data[1])) 
		  					{
			                    if (json_encode($data[1])!='"pagado"') 
			                    {
		                    ?>
			                    <div class="col-6 text-end">
			  						<button class="btn btn-success startPay"><i class="fa fa-credit-card"></i> Iniciar Pago</button>
			  					</div>
		                    <?php 
		                    	}
		                    }
		                    ?>
			  					<!-- <div class="col-6 text-end">
			  						<button class="btn btn-success" form="form"><i class="fa fa-credit-card"></i> Iniciar Pago</button>
			  					</div> -->
		  				</div>
		  			</div>
		  		</div>
  			</div>
	  	</div>
  	</div>
    
<script src="assets/js/jquery-3.2.1.min.js"></script>
<!-- <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script> -->
<!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script> -->
<script>
	var registro=<?php echo $registros; ?> ;

	console.log("<?php echo $dato; ?>");
	console.dir(registro['0'].cliente);

	$(document).ready( function () {
		load();
	});
	$('.startPay').on('click',function(){
		$('.overlayPagina').css('display','flex');
		$('#form').submit();
	});

	function load()
	{
		$('.name').html(registro['0'].clinomx);
		let direccion = registro['0'].caltip+' '+
			registro['0'].caldes+' '+
			registro['0'].prenro;
		$('.address').html(direccion);
		$('.measurer').html(registro['0'].FMedidor);

		$('.d1').html('S/ '+parseFloat(registro['0'].CtaCteSal.replace(',', '.')).toFixed(2));
		$('.d2').html('S/ '+parseFloat(registro['0'].CuotaFac.replace(',', '.')).toFixed(2));
		$('.d3').html('S/ '+parseFloat(registro['0'].FacTotal.replace(',', '.')).toFixed(2));

		let total = parseFloat(registro['0'].FacTotal.replace(',', '.'))+
			parseFloat(registro['0'].CuotaFac.replace(',', '.'))+
			parseFloat(registro['0'].CtaCteSal.replace(',', '.'));
		$('.numRecibo').html(registro['0'].FacSerNro+'-'+registro['0'].FacNro);
		$('.periodo').html("<?php echo $periodo; ?> ");
		$('.fechaVence').html("<?php echo $fechaVence; ?> ");
		$('.estadoRecibo').html(registro['1']=='pagado'?'<span class="badge bg-success">PAGADO</span>':'<span class="badge bg-warning text-dark">PENDIENTE</span>');
		$('.total').html(total.toFixed(2));

		$('.medidor').val(registro['0'].FMedidor);
		$('.id').val(registro['0'].id);
		// $('.inscripcion').val(registro['0'].InscriNrx);
		
		$('.test').html(registro['1']);
		$('.amount').val(total);
	}
</script>
</body>
</html>
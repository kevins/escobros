<?php
    include 'config/functions.php';
    include 'config/payload_.php';
    $con = new payload_();

    $transactionToken = $_POST["transactionToken"];
    $email = $_POST["customerEmail"];
    $amount = $_GET["amount"];
    $purchaseNumber = $_GET["purchaseNumber"];    
    $channel='';

    if ($channel == "pagoefectivo") {
        // CIP => $transactionToken
        // Registrar código CIP en su BD y asociarlo al pedido
        $url = $_POST["url"];
        header('Location: '.$url);
        exit;
    } else {   
        $token = generateToken();
        $data = generateAuthorization($amount, $purchaseNumber, $transactionToken, $token);

        if (isset($data->dataMap)) 
        {
            if ($data->dataMap->ACTION_CODE == "000") 
            {
                // echo 'algo tiene q hacer--..';
                $c = preg_split('//', $data->dataMap->TRANSACTION_DATE, -1, PREG_SPLIT_NO_EMPTY);
                $fechaHora = $c[4].$c[5]."/".$c[2].$c[3]."/".$c[0].$c[1]." ".$c[6].$c[7].":".$c[8].$c[9].":".$c[10].$c[11];
                $con->savePay($_GET["id"],$data->dataMap->ID_UNICO,$_GET["inscripcion"],$_GET["medidor"],$purchaseNumber,$fechaHora,$data->dataMap->CARD,$_GET["deudaTotal"],$data->order->amount,$data->dataMap->ACTION_DESCRIPTION,$_GET["fechaPago"],1);
            }
        }
        else
        {
            $c = preg_split('//', $data->data->TRANSACTION_DATE, -1, PREG_SPLIT_NO_EMPTY);
            $fechaHora = $c[4].$c[5]."/".$c[2].$c[3]."/".$c[0].$c[1]." ".$c[6].$c[7].":".$c[8].$c[9].":".$c[10].$c[11];
            // $con->savePay($_GET["id"],$data->data->ID_UNICO,$_GET["inscripcion"],$_GET["medidor"],$purchaseNumber,$fechaHora,$data->data->CARD,'--',$data->data->ACTION_DESCRIPTION,0);
            $con->savePay($_GET["id"],$data->data->ID_UNICO,$_GET["inscripcion"],$_GET["medidor"],$purchaseNumber,$fechaHora,$data->data->CARD,$_GET["deudaTotal"],'--',$data->data->ACTION_DESCRIPTION,$_GET["fechaPago"],0);
            // echo 'algo paso';
        }
        
    }
    // echo  json_encode($data);
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
     <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" href="plugins/fontawesome/all/all.min.css"/>
    <link rel="stylesheet" href="asset/main.css">
    <title>EMUSAP S.A</title>
  </head>
  <body style="background: url('img/bgg.jpg') repeat scroll 50% 0;">
    <?php
    include 'plantilla/header.php';
    ?>
    <div class="container-fluid" style="height: 100vh; z-index: 1111;">
        <div class="row justify-content-center" style="height: 100vh;">
            <div class="col-md-8">
                <div class="card shadow-lg">
                    <div class="card-header">
                        <div class="card-title m-0">
                            <h3 class="m-0"><i class="fa fa-money-check"></i> Estado del pago</h3>
                        </div>
                    </div>
                    <div class="card-body">
                        <?php 
                        if (isset($data->dataMap)) 
                        {
                            if ($data->dataMap->ACTION_CODE == "000") 
                            {
                                $c = preg_split('//', $data->dataMap->TRANSACTION_DATE, -1, PREG_SPLIT_NO_EMPTY);
                        ?>
                            <div class="alert alert-success text-center" role="alert">
                                <b><?php echo $data->dataMap->ACTION_DESCRIPTION;?></b>                  
                            </div>
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <b>Número de pedido: </b> <?php echo $purchaseNumber; ?>
                                    </div>
                                    <div class="col-md-12">
                                        <b>Fecha y hora del pedido: </b> <?php echo $c[4].$c[5]."/".$c[2].$c[3]."/".$c[0].$c[1]." ".$c[6].$c[7].":".$c[8].$c[9].":".$c[10].$c[11]; ?>
                                    </div>
                                    <div class="col-md-12">
                                        <b>Tarjeta: </b> <?php echo $data->dataMap->CARD." (".$data->dataMap->BRAND.")"; ?>
                                    </div>
                                    <div class="col-md-12">
                                        <b>Importe pagado: </b> <?php echo $data->order->amount. " ".$data->order->currency; ?>
                                    </div>
                                </div>
                            </div>
                        <?php
                            }
                        } 
                        else 
                        {
                            $c = preg_split('//', $data->data->TRANSACTION_DATE, -1, PREG_SPLIT_NO_EMPTY);
                            ?>
                            <div class="alert alert-danger text-center" role="alert">
                                <b><?php echo $data->data->ACTION_DESCRIPTION;?></b>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <b>Número de pedido: </b> <?php echo $purchaseNumber; ?>
                                </div>
                                <div class="col-md-12">
                                    <b>Fecha y hora del pedido: </b> <?php echo $c[4].$c[5]."/".$c[2].$c[3]."/".$c[0].$c[1]." ".$c[6].$c[7].":".$c[8].$c[9].":".$c[10].$c[11]; ?>
                                </div>
                                <div class="col-md-12">
                                    <b>Tarjeta: </b> <?php echo $data->data->CARD." (".$data->data->BRAND.")"; ?>
                                </div>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                    <div class="card-footer text-center">
                        <form id="showTicket" action="recibo.php" method="post">
                            <input type="hidden" id="medidor" name="medidor" value="<?php echo $_GET["medidor"]; ?>">
                            <input type="hidden" id="id" name="id" value="<?php echo $_GET["id"]; ?>">
                        </form>
                    <?php 
                    if (isset($data->dataMap)) 
                    {
                        if ($data->dataMap->ACTION_CODE == "000") 
                        {
                    ?>
                    <!-- <div class="card-footer text-center"> -->
                        <button class="btn btn-success" form="showTicket"><i class="fa fa-file"></i> Ver recibo</button>
                    <!-- </div> -->
                    <?php 
                        }
                    }
                    ?>
                    <a href="/escobros" class="btn btn-success"><i class="fa fa-cart-shopping"></i> Realizar otro pago</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <hr>
    <div class="container">
        <div class="alert alert-success" role="alert">
            Aprobado y completado con exito                        
        </div>

        <div class="row">
            <div class="col-md-12">
                <b>Número de pedido: </b> 3412312425                            
            </div>
            <div class="col-md-12">
                <b>Fecha y hora del pedido: </b> 12/10/22 09:28:00                            
            </div>
            <div class="col-md-12">
                <b>Tarjeta: </b> 447411******2240 (visa)                            
            </div>
            <div class="col-md-12">
                <b>Importe pagado: </b> 165.99 PEN                            
            </div>
        </div>
    </div> -->
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <!-- <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script> -->
     <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
  </body>
</html>
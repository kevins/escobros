
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
     <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" href="plugins/fontawesome/all/all.min.css"/>
    <link rel="stylesheet" href="asset/main.css">
    <title>EMUSAP S.A</title>
  </head>
  <body style="background: url('img/bgg.jpg') repeat scroll 50% 0;">
  	<?php
	include 'plantilla/header.php';
	?>
  	<div class="container-fluid" style="height: 100vh; z-index: 1111;">
  		<div class="row justify-content-center" style="height: 100vh;">
  			<div class="col-md-8">
	  			<div class="card shadow-lg">
					<div class="card-header">
						<div class="card-title m-0">
							<h3 class="m-0"><i class="fa fa-money-check"></i> Estado del pago</h3>
						</div>
					</div>
		  			<div class="card-body">
		  				<div class="alert alert-success text-center" role="alert">
				        	<b>Aprobado y completado con exito</b>                      
				    	</div>
		  				<div class="container">
						    <div class="row">
						        <div class="col-md-12">
						            <b>Número de pedido: </b> 3412312425                            </div>
						        <div class="col-md-12">
						            <b>Fecha y hora del pedido: </b> 12/10/22 09:28:00                            </div>
						        <div class="col-md-12">
						            <b>Tarjeta: </b> 447411******2240 (visa)                            </div>
						        <div class="col-md-12">
						            <b>Importe pagado: </b> 165.99 PEN                            </div>
						    </div>
					    </div>
		  			</div>
		  			<div class="card-footer text-center">
		  				<button class="btn btn-success btnSubmit"><i class="fa fa-file"></i> Ver recibo</button>
		  			</div>
		  		</div>
  			</div>
	  	</div>
  	</div>
  	<hr>
  	<div class="container">
		<div class="alert alert-success" role="alert">
        	Aprobado y completado con exito                        
    	</div>

	    <div class="row">
	        <div class="col-md-12">
	            <b>Número de pedido: </b> 3412312425                            </div>
	        <div class="col-md-12">
	            <b>Fecha y hora del pedido: </b> 12/10/22 09:28:00                            </div>
	        <div class="col-md-12">
	            <b>Tarjeta: </b> 447411******2240 (visa)                            </div>
	        <div class="col-md-12">
	            <b>Importe pagado: </b> 165.99 PEN                            </div>
	    </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <!-- <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script> -->
     <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
    
  </body>
</html>
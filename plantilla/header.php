
<?php include 'config/config.php'; ?>
<style>
	.img-logo
	{
		width: 35%;
    	height: 90px;
    	border-radius: 50%;
	}
	.letter-logo{
		color: #2b50d5;
	    font-weight: bold;
	    text-shadow: 1px 2px rgb(178 210 225);
	}
	nav{
		z-index: 111;
	}
	.bg-nav{
		/*background-color: rgb(248 249 250 / 30%)!important;*/
	}
</style>
<nav class="navbar navbar-light bg-nav">
  	<div class="container-fluid">
    	<a class="navbar-brand" href="/escobros">
      		<img src="img/logo-rounded.jpg" class="img-logo shadow">
      		<span class="letter-logo">Emusap Abancay S.A.</span>
    	</a>
  	</div>
</nav>
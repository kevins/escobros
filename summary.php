<?php
include 'config/functions.php';
include 'config/payload_.php';
// $amount = 1;
// ---------------------------------------
$mes = array(
    "01" => "ENERO","02" => "FEBRERO",
    "03" => "MARZO","04" => "ABRIL",
    "05" => "MAYO","06" => "JUNIO",
    "07" => "JULIO","08" => "AGOSTO",
    "09" => "SETIEMBRE","10" => "OCTUBRE",
    "11" => "NOVIEMBRE","12" => "DICIEMBRE"
);
$pay = new payload_();
	// echo $_POST['medidor'];
	$registro_ = $pay->show($_POST['id']);
	$registro = json_encode($registro_);
	$ban = count($registro_);

	// echo $_POST['amount'];
	// echo ('------------------');
	$onlyDate = explode('/',explode('-',str_replace(' ', '-', trim($registro_[0]['FacEmiFec'])))[0]);
	$periodo = date(trim($registro_[0]['FacEmiFec']));//aka esta la fecha real
	$fechaPago = $mes[$onlyDate[1]].' '.$onlyDate[2];

// echo $registro_[0]['FacTotal'];
// echo ('<br>');
// echo $registro_[0]['CuotaFac'];
// echo ('<br>');
// echo $registro_[0]['CtaCteSal'];
// echo ('<br>');
// echo ('--------------------------------<br>');
	// ESTA OPERACION NO LO REALIZA CORRECTAMENTE
	$montoTotal = (float) $registro_[0]['FacTotal']+(float) $registro_[0]['CuotaFac']+(float) $registro_[0]['CtaCteSal'];
	$montoTotal = $_POST['amount'];
	// echo $montoTotal;
	// echo ('<br>');
	$comision = $montoTotal*(3.45/100);
	// echo $comision;
	// echo ('<br>');
	$igv = $comision*0.18;
	$comisionGestion = 0.71;
	// echo $igv;
	// echo ('<br>');
	$montoIncluidoComision = $montoTotal+round($comision, 2)+round($igv, 2)+$comisionGestion;
	$montoRedondeado = round($montoIncluidoComision, 2);

	$amount = (float) $montoRedondeado;
	// echo $amount;
	// echo ('<br>');
	$detallePago = "Detalle de pago";
	$token = generateToken();
	// echo json_encode($token);
	$email = $registro_[0]['CorEleCli'];
	$sesion = generateSesion($amount, $token, $email);
	$purchaseNumber = generatePurchaseNumber();

	// if($registro_[0]['deuda'])
	// echo $registro_[0]['deuda'];
	// echo $ban;
	// echo json_encode($registro);
	// echo $reg->inscripcion;

	$medidor = $_POST['medidor'];
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
     <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" href="plugins/fontawesome/all/all.min.css"/>
    <link rel="stylesheet" href="asset/main.css">
    <title>EMUSAP S.A</title>
    <link rel="stylesheet" href="assets/css/main.css">
</head>
  <body style="background: url('img/bgg.jpg') repeat scroll 50% 0;">
  	<?php
	include 'plantilla/header.php';
	?>
	<div class="overlayPagina">
	    <div class="loadingio-spinner-spin-i3d1hxbhik m-auto">
	        <div class="ldio-onxyanc9oyh">
	            <div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div>
	        </div>
	    </div>
	</div>
  	<div class="container-fluid" style="height: 100vh;">
  		<div class="row justify-content-center" style="height: 100vh;">
  			<div class="col-md-8">
  				<div class="row my-3">
  					<div class="col-4  text-start">
						<span class="badge bg-secondary customer-timeline text-white">1</span>
						<span class="fw-bold">Consulta</span>
					</div>
					<div class="col-4  text-center">
						<span class="badge bg-secondary customer-timeline text-white">2</span>
						<span class="fw-bold">Verifica</span>
					</div>
					<div class="col-4  text-end">
						<span class="badge bg-primary customer-timeline text-white">3</span>
						<span class="fw-bold">Confirma y Paga</span>
					</div>
	  			</div>
	  			<div class="card shadow-lg">
					<div class="card-header">
						<div class="card-title m-0">
							<h3 class="m-0"><i class="fa fa-cart-shopping"></i> Pago de recibo</h3>
						</div>
					</div>
		  			<div class="card-body">
		  				<form id="formQuery" action="check.php" method="post">
  							<input type="hidden" id="dato" name="dato">
  							<input type="hidden" name="rdTipoBusqueda" value="inscripcion">
  							
  						</form>
		  				<div class="alert alert-warning" style="display: <?php echo $ban==0?'block':'none'; ?>">
		  					<p class="m-0 text-center">No se encontro pagos pendientes con el numero <b><?php echo $dato; ?></b>.</p>
		  				</div>
		  				<div class="row">
		  					<div class="alert alert-primary">
		  						<p class="m-0 font-weight-bold"><strong><i class="fa fa-check"></i> Verifique sus datos y monto a pagar.</strong></p>
		  					</div>
		  				</div>
		  				<div class="row justify-content-center" style="display: <?php echo $ban==0?'none':'block'; ?>">
		  					<div class="alert alert-secondary">
		  						<h6 class="m-0"><b>Cliente:</b> <span class="name"></span></h6>
		  						<h6 class="m-0"><b>Direccion:</b> <span class="address"></span></h6>
		  						<h6 class="m-0"><b>Deuda total:</b> <span class="pay"></span></h6>
		  						<h6 class="m-0"><b>Gasto de operaciones:</b> <span class="commission"></span></h6>
		  						<h6 class="m-0"><b>Monto a pagar:</b> <span class="allPay"></span></h6>
		  					</div>
		  				</div>
		  				<div class="row">
		  					<div class="alert alert-warning">
		  						<p class="m-0 font-weight-bold"><strong><i class="fa fa-circle-info"></i> Durante el proceso serà redirigido; No cierre esta ventana hasta obtener el mensaje de confirmaciòn.</strong></p>
		  					</div>
		  				</div>
		  				<input type="checkbox" name="ckbTerms" id="ckbTerms" onclick="visaNetEc3()"> 
		  				<label for="ckbTerms">He leído y aceptado los 
		  					<button class="btn btn-secondary btn-sm py-0" data-bs-toggle="modal" data-bs-target="#modalTyc">Términos y condiciones</button>
		  				</label>
		  			</div>
		  			<div class="card-footer">
		  				<div class="row">
		  					<div class="col-6">
		  						<button class="btn btn-secondary" form="formQuery"><i class="fa fa-arrow-left"></i> Volver</button>
		  					</div>
		  					<div class="col-6">
		  						<!-- <button id="btn_pagar" class="btn btn-success float-end">REALIZAR PAGO</button> -->
		  						<form id="frmVisaNet" class="float-end" action="http://localhost/escobros/finalizar.php?amount=<?php echo $amount;?>&purchaseNumber=<?php echo $purchaseNumber?>&medidor=<?php echo $_POST['medidor']?>&fechaPago=<?php echo $fechaPago?>&id=<?php echo $_POST['id']?>&deudaTotal=<?php echo $montoTotal?>&inscripcion=<?php echo $registro_[0]['InscriNrx']?>">
							        <script src="<?php echo VISA_URL_JS?>" 
							            data-sessiontoken="<?php echo $sesion;?>"
							            data-channel="web"
							            data-merchantid="<?php echo VISA_MERCHANT_ID?>"
							            data-merchantlogo="http://localhost/escobros/img/logosf.png"
							            data-purchasenumber="<?php echo $purchaseNumber;?>"
							            data-amount="<?php echo $amount; ?>"
							            data-expirationminutes="5"
							            data-timeouturl="http://localhost/escobros/"
							        ></script>
							    </form>
		  					</div>
		  				</div>
		  			</div>
		  		</div>
  			</div>
	  	</div>
  	</div>
  	<div class="modal fade" id="modalTyc" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  	<div class="modal-dialog">
	    	<div class="modal-content">
	      		<div class="modal-header py-1">
	        		<h5 class="modal-title" id="exampleModalLabel">Terminons y condiciones</h5>
	        		<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      		</div>
	      		<div class="modal-body">
	    			<p class="fw-italic">Terminos y condiciones</p>
	    			<p class="fw-italic">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quibusdam quia praesentium, repellat cupiditate eos recusandae numquam, distinctio vel consectetur perspiciatis, rerum enim officiis porro molestias debitis dignissimos est dolorem ea.</p>
	      		</div>
	      		<div class="modal-footer py-1">
	        		<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
	      		</div>
	    	</div>
	  	</div>
	</div>

    
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="assets/js/jquery-3.2.1.min.js"></script>
<!-- <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script> -->
<!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script> -->
<script src="assets/js/bootstrap.bundle.min.js"></script>
<script>
	var frmVisa = document.getElementById('frmVisaNet');

	if(document.body.contains(frmVisa)) {
	    // document.getElementById('frmVisaNet').setAttribute("style", "display:none");
	    $('.start-js-btn').attr('disabled',true);
	}
	function visaNetEc3() {
	    if (document.getElementById('ckbTerms').checked) {
	        // document.getElementById('frmVisaNet').setAttribute("style", "display:auto");
	        $('.start-js-btn').attr('disabled',false);
	    } else {
	        // document.getElementById('frmVisaNet').setAttribute("style", "display:none");
	        $('.start-js-btn').attr('disabled',true);
	    }
	}
</script>
<script>
</script>
	<script>
		var registro=<?php echo $registro; ?> ;
		var montoRedondeado=<?php echo $montoRedondeado; ?> ;

		console.log("<?php echo $medidor; ?>");
		console.dir(registro['0'].cliente);

		$(document).ready( function () {
			load();
			$('.overlayPagina').css("display","none");
		});

		function load()
		{
			$('.name').html(registro['0'].clinomx);
			let direccion = registro['0'].caltip+' '+
				registro['0'].caldes+' '+
				registro['0'].prenro;
			$('.address').html(direccion);
			// let monto = parseFloat(registro['0'].FacTotal)+parseFloat(registro['0'].CuotaFac)+parseFloat(registro['0'].CtaCteSal);
			let monto = parseFloat(registro['0'].FacTotal.replace(',', '.'))+
				parseFloat(registro['0'].CuotaFac.replace(',', '.'))+
				parseFloat(registro['0'].CtaCteSal.replace(',', '.'));
			$('.pay').html(monto.toFixed(2));
			$('.commission').html((montoRedondeado-monto).toFixed(2));
			$('.allPay').html(montoRedondeado);
			$('#dato').val(registro['0'].InscriNrx);
		}
	</script>
  	</body>
</html>
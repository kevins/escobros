<?php

// include 'fpdf.php';
require('fpdf.php');
class conexion{
	private $servidor;
	private $usuario;
	private $contrasena;
	private $basedatos;
	public $conexion;
	public function __construct(){
	    $this->servidor = "localhost";
		$this->usuario = "root";
		$this->contrasena = "";
		$this->basedatos = "escobros";
	}
	function conectar(){
		$this->conexion = new mysqli($this->servidor,$this->usuario,$this->contrasena,$this->basedatos);
		$this->conexion->set_charset("utf8");
	}
	function cerrar(){
		$this->conexion->close();	
	}
}

class payload_
{
	private $conexion;
	function __construct()
	{
	   // require_once 'conexion.php';
	    $this->conexion = new conexion();
		$this->conexion->conectar();
	}
	function showPay($dato){
		$arreglo = array();
		if ($consulta = $this->conexion->conexion->query("select * from pagado where idRecibo='".$dato."' order by id desc limit 1;")) {
			while ($consulta_VU = mysqli_fetch_array($consulta)) {
				$arreglo[] = $consulta_VU;
			}
			return $arreglo;
			// return $consulta;
			$this->conexion->cerrar();	
		}
	}
	function showRecibo($dato){
		$arreglo = array();
		if ($consulta = $this->conexion->conexion->query("select * from recibo where id='".$dato."'")) {
			while ($consulta_VU = mysqli_fetch_array($consulta)) {
				$arreglo[] = $consulta_VU;
			}
			return $arreglo;
			// return $consulta;
			$this->conexion->cerrar();	
		}
	}
}

$mes = array(
    "01" => "ENERO","02" => "FEBRERO",
    "03" => "MARZO","04" => "ABRIL",
    "05" => "MAYO","06" => "JUNIO",
    "07" => "JULIO","08" => "AGOSTO",
    "09" => "SETIEMBRE","10" => "OCTUBRE",
    "11" => "NOVIEMBRE","12" => "DICIEMBRE"
);

// datos de usuario
$pay = new payload_();
$data = $pay->showRecibo($_POST['id']);
$numInscripcion=$data[0]['InscriNrx'];
$nombreCompleto=$data[0]['clinomx'];
$direccion=trim($data[0]['caltip']).' '.trim($data[0]['caldes']).' '.trim($data[0]['prenro']);
$numRecibo=trim($data[0]['FacSerNro']).'-'.trim($data[0]['FacNro']);

$onlyDate = explode('/',explode('-',str_replace(' ', '-', trim($data[0]['FacEmiFec'])))[0]);
$periodo = date(trim($data[0]['FacEmiFec']));//aka esta la fecha real solo pruebas
$periodo = $mes[$onlyDate[1]].' '.$onlyDate[2];
// datos del pago

$pagado = $pay->showPay($_POST['id']);
// echo($pagado[0]['fechaHora']);
$numeroPedido = $pagado[0]['numeroPedido']; 
$id = $pagado[0]['ID_UNICO']; 
$fechaHoraPago = $pagado[0]['fechaHora'];
$importePagado = 'S/. '.$pagado[0]['importePagado'];

$marco = 0;
$pdf = new FPDF('P','mm',array(100,150));
$pdf->AddPage();
$pdf->Cell(0,25,'',$marco,1,'C');
$pdf->Image('img/logosf.png' , 10 ,10, 80 , 25);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(0,5,'-------------------------------------------------------------------',$marco,1,'l');
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,10,'RECIBO DE PAGO',$marco,1,'C');
$pdf->SetFont('Arial','B',10);
$pdf->Cell(25,5,'Numero:',$marco,0,'l');
$pdf->Cell(0,5,$numeroPedido,$marco,1,'l');
$pdf->Cell(25,5,'Transaccion:',$marco,0,'l');
$pdf->Cell(0,5,$id,$marco,1,'l');
$pdf->Cell(25,5,'Fecha:',$marco,0,'l');
$pdf->Cell(0,5,$fechaHoraPago,$marco,1,'l');
$pdf->Cell(0,5,'-------------------------------------------------------------------',$marco,1,'l');
$pdf->Cell(25,5,'Inscripcion:',$marco,0,'l');
$pdf->Cell(0,5,$numInscripcion,$marco,1,'l');
$pdf->Cell(25,5,'Cliente:',$marco,0,'l');
$pdf->Cell(0,5,$nombreCompleto,$marco,1,'l');
$pdf->Cell(25,5,'Direccion:',$marco,0,'l');
$pdf->Cell(0,5,$direccion,$marco,1,'l');
$pdf->Cell(25,5,'Nro.Recibo:',$marco,0,'l');
$pdf->Cell(0,5,$numRecibo,$marco,1,'l');
$pdf->Cell(25,5,'Periodo:',$marco,0,'l');
$pdf->Cell(0,5,$periodo,$marco,1,'l');

$pdf->Cell(25,1,' ',$marco,0,'l');
$pdf->Cell(0,1,'-----------------------',$marco,1,'l');
$pdf->Cell(25,5,'Total:',$marco,0,'l');
$pdf->Cell(0,5,$importePagado,$marco,1,'l');
$pdf->Cell(25,1,' ',$marco,0,'l');
$pdf->Cell(0,1,'-----------------------',$marco,1,'l');

$pdf->Cell(25,5,'Cajero:',$marco,0,'l');
$pdf->Cell(0,5,'PAGO ONLINE',$marco,1,'l');
$pdf->Output();
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
     <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" href="plugins/fontawesome/all/all.min.css"/>
    <link rel="stylesheet" href="asset/main.css">
    <title>EMUSAP S.A</title>
  </head>
  <body style="background: url('img/bgg.jpg') repeat scroll 50% 0;">
  	<?php
	include 'plantilla/header.php';
	?>
	
	<!-- <div class="background-emp">
		<img src="img/planta.jpg" alt="">
	</div> -->
  	<div class="container-fluid" style="height: 100vh; z-index: 1111;">
  		<div class="row justify-content-center" style="height: 100vh;">
  			<div class="col-md-8">
  				<div class="row my-3" style="z-index: 1111;">
  					<div class="col-4  text-start">
						<span class="badge bg-primary customer-timeline text-white">1</span>
						<span class="fw-bold">Consulta</span>
					</div>
					<div class="col-4  text-center">
						<span class="badge bg-secondary customer-timeline text-white">2</span>
						<span class="fw-bold">Verifica</span>
					</div>
					<div class="col-4  text-end">
						<span class="badge bg-secondary customer-timeline text-white">3</span>
						<span class="fw-bold">Confirma y Paga</span>
					</div>
	  			</div>
	  			<div class="card shadow-lg">
					<div class="card-header">
						<div class="card-title m-0">
							<h3 class="m-0"><i class="fa fa-search"></i> Consulta tu recibo</h3>
						</div>
					</div>
		  			<div class="card-body">
		  				<form id="formQuery" action="check.php" method="post">
		  				<div class="row justify-content-center">
			  				<div class="col-md-7">
			  					<div class="align-content-center text-center justify-content-center">
			  						<div class="custom-radio custom-control custom-control-inline me-3" style="display: inline;">
			  							<input type="radio" id="rd3" name="rdTipoBusqueda" class="custom-control-input" checked value="inscripcion">
			  							<label class="custom-control-label" for="rd3"># Inscripcion</label>
			  						</div>
			  						<div class="custom-radio custom-control custom-control-inline me-3" style="display: inline;">
			  							<input type="radio" id="rd2" name="rdTipoBusqueda" class="custom-control-input" value="medidor">
			  							<label class="custom-control-label" for="rd2"># Medidor</label>
			  						</div>
			  					</div>
			  				</div>
			  				<!-- <div class="col-md-7 mt-3">
			  					<input type="text" name="dato" class="form-control form-control-name soloNumeros dato" placeholder="Ingrese la informacion. . ." autocomplete="no" maxlength="8">
			  				</div> -->
			  				<div class="col-md-7 mt-3">
			  					<div class="input-group mb-3">
								  	<!-- <input type="text" class="form-control" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="button-addon2"> -->
								  	<input type="text" name="dato" class="form-control form-control-name soloNumeros dato" placeholder="Ingrese la informacion. . ." autocomplete="no" maxlength="8">
								  	<button class="btn btn-primary" type="button" id="button-addon2" title="Donde encontrar los datos para realizar busqueda." data-bs-toggle="modal" data-bs-target="#modalRecibo"><span class="fa fa-question-circle text-white"></span></button>
								</div>
			  				</div>		
		  				</div>
		  				</form>
		  			</div>
		  			<form id="showTicket" action="recibo.php" method="post">
		  				<input type="hidden" name="medidor" value="DA19109288">
		  			</form>
		  			<div class="card-footer text-center">
		  				<button class="btn btn-success btnSubmit"><i class="fa fa-search"></i> Consultar Recibo</button>
		  				<!-- <p class='demo'><a href='recibo.php' target='_blank' class='demo btn btn-danger'>[Demo]</a></p> -->
		  				<button class="btn btn-success" form="showTicket" style="display: none;">[Demo]</button>
		  				<a href="http://localhost/escobros/test.php?amount=12&purchaseNumber=212121&id=19431" class="btn btn-danger btn-sm" target="_blank" style="display: none;">enlace</a>
		  			</div>
		  		</div>
		  		<div class="alert alert-primary bg-primary mt-3 shadow-lg text-white">
		  			<h4><i class="fa fa-circle-info"></i> Recomendaciones para pagos desde banca móvil</h4>
		  			<p class="text-white m-0"><b>Interbank:</b> Puede realizar el pago, buscandonos en <span class="badge bg-light text-dark">Pago de Servicios</span> desde la aplicacion.</p>
		  			<p class="text-white m-0"><b>Caja Cusco:</b> Puede realizar el pago, buscandonos en <span class="badge bg-light text-dark">Pago de Servicios</span> desde la aplicacion.</p>
		  			<p class="text-white m-0"><b>Interbank:</b> Puede escanear el <span class="badge bg-light text-dark">CODIGO QR</span> directamente desde la aplicacion.</p>
		  			<p class="text-white m-0"><b>Caja Cusco:</b> Puede escanear el <span class="badge bg-light text-dark">CODIGO QR</span> directamente desde la aplicacion.</p>
		  		</div>
  			</div>
	  	</div>
  	</div>
  	<div class="modal fade" id="modalRecibo" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header py-1">
	        <h5 class="modal-title" id="exampleModalLabel">Modelo de recibo</h5>
	        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      </div>
	      <div class="modal-body">
	    	<img src="img/recibopartes.png" class="w-100">
	      </div>
	      <div class="modal-footer py-1">
	        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
	      </div>
	    </div>
	  </div>
	</div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="assets/js/jquery-3.2.1.min.js"></script>
    <!-- <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script> -->
     <script src="assets/js/bootstrap.bundle.min.js"></script>
    
	<script>
		// $('.soloNumeros').on('input', function () { 
		//     this.value = this.value.replace(/[^0-9]/g,'');
		// });
		$('.btnSubmit').on('click',function(){
			if($('.dato').val()=='') alert('Ingrese el numero de inscripcion o medidor');
			else $('#formQuery').submit();
		});
		$('input[name=rdTipoBusqueda]').on('click', function () { 
		    // alert($(this).attr('id'));
		    $('.dato').val('');
		    if($(this).attr('id')=='rd3')
		    {
		    	$('.dato').addClass('soloNumeros');
		    	$('.dato').attr('maxlength','8');
		    	
		    }
		    else
		    {
		    	$('.dato').removeClass('soloNumeros');
		    	$('.dato').attr('maxlength','10');
		    }
		});
	</script>
  </body>
</html>